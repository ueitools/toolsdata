using System;
using System.Collections.Generic;
using System.IO;

namespace ToolsData {
    [Serializable]
    public class Id {
        public static readonly Id INVALID_ID = new Id();

        private char _mode;
        private int _number;

        private Id() {
            _mode = 'Z';
            _number = -9999;
        }

        public Id(string id) {
            string value = id.Trim();

            if (value == string.Empty) {
                throw new FormatException("Empty value not a valid ID");
            }

            if (ValidateIdFormat(id) == false) {
                throw new FormatException(string.Format("Id {0} is invalid as an ID.", id));
            }

            _mode = char.ToUpper(id[0]);
            _number = int.Parse(id.Substring(1));
        }

        public char Mode {
            get { return _mode; }
        }

        public int Number {
            get { return _number; }
            set { _number = value; }
        }

        public static bool ValidateIdFormat(string id) {
            if (id == string.Empty) {
                return true;
            }

            if (char.IsLetter(id[0]) == false) {
                return false;
            }

            try {
                int.Parse(id.Substring(1));
            } catch {
                return false;
            }

            return true;
        }

        private static bool IsNull(object obj) {
            return obj == null;
        }

        private static bool IsValidIdObject(object obj) {
            return null != obj && obj.GetType() == typeof(Id);
        }

        public static bool operator==(Id idLeft, Id idRight) {
            if (IsNull(idLeft) && IsNull(idRight)) {
                return true;
            }

            if (IsValidIdObject(idLeft) == false || IsValidIdObject(idRight) == false) {
                return false;
            }

            return idLeft.Mode == idRight.Mode && idLeft.Number == idRight.Number;
        }

        public static bool operator !=(Id idLeft, Id idRight) {
            return !(idLeft == idRight);
        }

        public static bool operator<=(Id idLeft, Id idRight) {
            if (IsValidIdObject(idLeft) == false || IsValidIdObject(idRight) == false) {
                return false;
            }
            
            if (idLeft.Mode < idRight.Mode) {
                return true;
            }

            if (idLeft.Mode == idRight.Mode) {
                return idLeft.Number <= idRight.Number;
            }

            return false;
        }

        public static bool operator >=(Id idLeft, Id idRight) {
            if (IsValidIdObject(idLeft) == false || IsValidIdObject(idRight) == false) {
                return false;
            }
            
            if (idLeft.Mode > idRight.Mode) {
                return true;
            }

            if (idLeft.Mode == idRight.Mode) {
                return idLeft.Number >= idRight.Number;
            }

            return false;
        }

        public static explicit operator string(Id id) {
            if (id == null) {
                return null;
            }

            return id.Get4DigitId();
        }

        public string Get3DigitId()
        {
            if (this == INVALID_ID)
                return null;

            return string.Format("{0}{1:D3}", _mode, _number).ToUpper();
        }

        public string Get4DigitId()
        {
            if (this == INVALID_ID)
                return null;

            return string.Format("{0}{1:D4}", _mode, _number).ToUpper();
        }

        [Serializable]
        public class List : List<Id> {
            public List() { }
            public List(IEnumerable<string> idList) {
                foreach (string id in idList) {
                    Add(new Id(id));
                }
            }

            public List(IEnumerable<Id> idList) : base(idList) {}

            public void LoadFile(string filename) {
                if (File.Exists(filename) == false) {
                    throw new FileNotFoundException(filename + " cannot be found.");
                }

                Clear();

                TextReader textReader = new StreamReader(filename);
                string allIds = textReader.ReadToEnd();
                textReader.Close();

                string[] idValues = allIds.Split(' ', '\n', '\r');
                foreach (string idValue in idValues) {
                    if (idValue == string.Empty) {
                        continue;
                    }
                    try {
                        Id id = new Id(idValue);
                        Add(id);
                    } catch(Exception ex) {
                        // log error?
                        string result = ex.Message;
                    }

                }
            }

            public bool Contains(Id findId)
            {
                foreach (Id id in this)
                {
                    if (id.Mode == findId.Mode &&
                        id.Number == findId.Number)
                        return true;
                }
                return false;
            }
        }

        public static bool TryParse(string idName, out Id id)
        {
            try
            {
                id = new Id(idName);
            }
            catch (FormatException)
            {
                id = null;
                return false;
            }

            return true;
        }
    }
}
