using System;
using System.Collections.Generic;
using System.IO;

namespace ToolsData {
    public class IdFilterConditions {
        public delegate bool ValidateIdFormatDelegate(string id);
        public delegate string FormatIdDelegate(string id);

        public enum FilterByType {
            None,
            Range,
            Mode,
            File
        }

        private FilterByType _filterType;
        private IdFilterRange _filterRange;
        private List<char> _filterModes;
        private string _filterIdsFilename;
        private Id.List _filterIdsFromFile;

        public IdFilterConditions() {
            _filterType = FilterByType.None;
            _filterRange = new IdFilterRange();
            _filterModes = null;
            _filterIdsFromFile = new Id.List();
        }

        public FilterByType FilterType {
            get { return _filterType; }
            set { _filterType = value; }
        }

        public IdFilterRange FilterRange {
            get {
                if (_filterRange == null) {
                    _filterRange = new IdFilterRange();
                }
                return _filterRange;
            }
        }

        public List<char> FilterModes {
            get {
                if (_filterModes == null) {
                    _filterModes = new List<char>();
                }
                return _filterModes;
            }
            set { _filterModes = value; }
        }

        public string FilterIdsFilename {
            get { return _filterIdsFilename; }
            set { _filterIdsFilename = value; }
        }

        public Id.List FilterIdsFromFile {
            get {
                if (_filterIdsFromFile == null) {
                    _filterIdsFromFile = new Id.List();
                }
                return _filterIdsFromFile;
            }
        }

        public void SetFilterRangeFrom(string fromId) {
            if (string.IsNullOrEmpty(fromId) == false) {
                _filterRange.SetFrom(new Id(fromId));
            }
        }

        public void SetFilterRangeTo(string toId) {
            if (string.IsNullOrEmpty(toId) == false) {
                _filterRange.SetTo(new Id(toId));
            }
        }

        public void LoadFile(string filename) {
            _filterIdsFilename = filename;
            _filterIdsFromFile.LoadFile(filename);
        }

        public class IdFilterRange {
            private Id _from;
            private Id _to;

            public IdFilterRange() {
                _from = Id.INVALID_ID;
                _to = Id.INVALID_ID;
            }

            public Id From {
                get {
                    if (_from == Id.INVALID_ID) {
                        return null;
                    }
                    return _from;
                }
            }

            public Id To {
                get {
                    if (_to == Id.INVALID_ID) {
                        return null;
                    }
                    return _to;
                }
            }

            public bool IsInRange(Id id) {
                if (_to == Id.INVALID_ID) {
                    return id == _from;
                }

                return (id >= _from && id <= _to);
            }

            internal void SetFrom(Id id) {
                if (_to != null && (string)_to != (string)Id.INVALID_ID) {
                    if (id.Mode != _to.Mode) {
                        throw new InvalidDataException("'From' Id differ in mode to the 'To' Id.");
                    }
                    if (id.Number > _to.Number) {
                        throw new InvalidDataException("'From' Id's number is greater than 'To' Id's number.");                        
                    }
                }

                _from = id;
            }

            internal void SetTo(Id id) {
                if (_from != null && (string)_from != (string)Id.INVALID_ID) {
                    if (id.Mode != _from.Mode) {
                        throw new InvalidDataException("'To' Id differ in mode to the 'From' Id.");
                    }
                    if (id.Number < _from.Number) {
                        throw new InvalidDataException("'To' Id's number is greater than 'From' Id's number.");
                    }
                }

                _to = id;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idList"></param>
        /// <returns></returns>
        public Id.List FilterList(Id.List idList)
        {
            Id.List ResultList = new Id.List();
            foreach (Id id in idList)
            {
                if (IsInRange(id))
                    ResultList.Add(id);
            }
            return ResultList;
        }

        public bool IsInRange(Id id)
        {
            switch (FilterType)
            {
                case FilterByType.Mode:
                    return IsInRangeByModes(id);
                case FilterByType.Range:
                    return IsInRangeByRange(id);
                case FilterByType.File:
                    return IsInRangeIdsFromFile(id);
                default:
                    return false;
            }
        }

        public bool IsInRange(string idName)
        {
            Id id = new Id(idName);
            return IsInRange(id);
        }

        private bool IsInRangeByModes(Id id)
        {
            return FilterModes.Contains(id.Mode);
        }

        private bool IsInRangeByRange(Id id)
        {
            if (FilterRange.From == null && FilterRange.To == null)
                return false;

            Id start = (FilterRange.From != null) ? FilterRange.From : FilterRange.To;
            Id end = (FilterRange.To != null) ? FilterRange.To : FilterRange.From;

            if (start.Mode != id.Mode || id.Number < start.Number)
                return false;
            if (id.Number > end.Number)
                return false;

            return true;
        }

        private bool IsInRangeIdsFromFile(Id id)
        {
            return FilterIdsFromFile.Contains(id);
        }
    }
}
